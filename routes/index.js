var express = require('express');
var router = express.Router();

var current_commit = require('child_process').execSync('git rev-parse HEAD').toString().trim().slice(0, 8);

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'xDrixxyz', commit: current_commit });
});

module.exports = router;
