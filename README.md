xsite

v7 rewrite of my site, xdrixxyz.ml

## Running
Prerequisites:
```
- Node.js v10.6.0
- Npm v6.1.0
- Git
```

Commands:
```
$ git clone https://gitlab.com/xdrixxyz/xsite
$ cd xsite
$ npm i
$ node ./bin/www
```
This will start a Express server on port 3000 unless defined otherwise either in the `./bin/www` file or via the PORT environment variable.

If you want the benefits of SSL, CloudFlare protection, and more then you need to setup Nginx, 
and get an SSL certificate and key to install on your origin.

I will not be sharing the Nginx config I use in this public repository, I have it backed up in a private snippet.

I also will not be showing you how to setup Nginx and SSL.

## FAQ

**Why another rewrite?**
To fix the bugs which were present in the old build.
And to add new features which we previously wouldn't be able to do.

**What super cool technologies does this rewrite use?**
This rewrite cannot run on a static HTML server. You need to be able to run Node.js Express apps to run this rewrite.

I use the following on the live production server for xdrixxyz.ml:
- This rewrite
- Nginx as a reverse proxy
- CloudFlare for DDoS protection, caching, firewall, etc
- Google Cloud Platform as my hosting provider
- Node.js version 10.6.0
- Npm version 6.1.0

**What are the differences between this rewrite and the v6 rewrite?**
- This rewrite has better support for paths, unlike the v6 rewrite, where visiting a path took you to the homepage no matter what.
- This rewrite has better support for CloudFlare's functionality as it's not a statically hosted webpage.
- This rewrite has new custom 404, 500, and maintenance error pages.
- This rewrite lets me add whatever I want, static or not, whenever I want.
- This rewrite taught me about Nginx configs

**Why do you still use the Pepperoni class names in your CSS/Pug/HTML/etc?**
Because I was too busy and too hyped to think of a new name so I just reused the Pepperoni class names.

**Why Express.js? Why not PHP? Heck, why can't you just host it statically using static HTML?**
Because:
- PHP, ew no
- Static HTML is a pain in the ass to maintain compared to mostly JavaScript/Pug which is easier to read, and easier to maintain